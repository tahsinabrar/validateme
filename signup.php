<!DOCTYPE html>
<html>
<head>
    <title>Signup confirmation</title>
</head>
<body style="width: 760px; margin: 0 auto;">

<?php

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    include 'db.php';

/*    var_dump($_POST);
    die();*/

    try {


        // prepare sql and bind parameters
        $stmt = $conn->prepare("INSERT INTO $tableName (name, email, username, password, age, gender, language1, language2, language3, country, address)
        VALUES (:name, :email, :username, :password, :age, :gender,:language1,:language2,:language3, :country, :address)");
        $stmt->bindParam(':name', $name);
        $stmt->bindParam(':email', $email);
        $stmt->bindParam(':username', $uName);
        $stmt->bindParam(':password', $pwd);
        $stmt->bindParam(':age', $age);
        $stmt->bindParam(':gender', $gender);
        $stmt->bindParam(':language1', $language1);
        $stmt->bindParam(':language2', $language2);
        $stmt->bindParam(':language3', $language3);
        $stmt->bindParam(':country', $country);
        $stmt->bindParam(':address', $address);

        // insert a row
        $name           = $_POST['name'];
        $email          = $_POST['email'];
        $uName          = $_POST['u_name'];
        $pwd            = $_POST['pass'];
        $age            = $_POST['age'];
        $gender         = $_POST['gender'];
        if(isset($_POST['language1'])) {
            $language1      = $_POST['language1'];
        } else {
            $language1      = '';
        }
        if(isset($_POST['language2'])) {
            $language2      = $_POST['language2'];
        } else {
            $language2      = '';
        }
        if(isset($_POST['language3'])) {
            $language3      = $_POST['language3'];
        } else {
            $language3      = '';
        }

        $country        = $_POST['country'];
        $address        = $_POST['myaddress'];
        $stmt->execute();
?>

<h3>New User created successfully.</h3>
<a href="index.php">Add another user</a>

<?php    } catch(PDOException $e) {
          echo "Request failed: " . $e->getMessage();
        // try {
            
            // include 'create_table.php';


            // prepare sql and bind parameters
        /*
            $stmt = $conn->prepare("INSERT INTO $tableName (name, email, username, password, age, gender, language, country, address)
            VALUES (:name, :email, :username, :password, :age, :gender,:language, :country, :addre)");
            $stmt->bindParam(':name', $name);
            $stmt->bindParam(':email', $email);
            $stmt->bindParam(':username', $username);
            $stmt->bindParam(':password', $password);
            $stmt->bindParam(':age', $age);
            $stmt->bindParam(':gender', $gender);
            $stmt->bindParam(':language', $language);
            $stmt->bindParam(':country', $country);
            $stmt->bindParam(':address', $address);

            // insert a row
            $name           = $_POST['name'];
            $email          = $_POST['email'];
            $username       = $_POST['username'];
            $password       = $_POST['password'];
            $age            = $_POST['age'];
            $gender         = $_POST['gender'];
            $language       = $_POST['language'];
            $country        = $_POST['country'];
            $address        = $_POST['address'];
            $stmt->execute();

            echo "New records created successfully.<br/>";
        */           
        /*
            } catch (PDOException $e) {
          echo "Request failed: " . $e->getMessage();
        }*/
    }
    $conn = null;

    // include 'details.php';

} else {
    echo "You are not allowed to play around here....";
}
?>
</body>
</html>
