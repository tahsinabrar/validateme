
<h2 style="text-align:center;">User Registration</h2>
<!-- Creating Form -->
<form method="POST" action="signup.php" name="form1">

    <!-- Creating Table, having 11rows and 2 columns. -->
    <table border="2" align="center" cellpadding="7">

        <!-- Start First Row -->
        <tr>
            <!-- Creating First Column -->
            <td><strong>Name:</strong></td>

            <!-- Creating Second Columns -->
            <td>
                <!-- TextBox -->
                <input type="text" name="name" required="required" />
            </td>
            <!-- Close First Row -->
        </tr>

        <!-- Creting First Columns -->
        <td><strong>Email:</strong></td>

        <!-- Creating Second Columns -->
        <td>
            <!-- Textbox -->
            <input type="email" name="email" id="email" required="required" />
        </td>
        <!-- Close Second row -->
        </tr>

        <tr>
            <td><strong>Age:</strong></td>
            <td>
                <!-- Textbox -->
                <input type="number" name="age" size="2" id="age" required="required" />
                <br>
                <span style="color: red; font-size: 12px;" id="ageError"></span>
            </td>
        </tr>

        <tr>
            <td><strong>Gender:</strong></td>
            <td>
                <!-- Radio Butong -->
                <input type="radio" name="gender" value="Male" required="required" />Male
                <input type="radio" name="gender" value="Female" required="required" />Female

            </td>
        </tr>

        <tr>
            <td><strong>Language:</strong></td>
            <td>
                <!-- Check box -->
                <input type="checkbox" name="language1" value="Hindi"/>Hindi
                <input type="checkbox" name="language2" value="English"/>English
                <input type="checkbox" name="language3" value="Urdu"/>Urdu
            </td>
        </tr>

        <tr>
            <td><strong>Country:</strong></td>
            <td>
                <!-- Combo Box -->
                <select name="country" required="required">
                <option value="" selected/>
                --Select--
                <option value="india"/>
                India
                <option value="pakistan"/>
                Pakistan
                <option value="bangladesh"/>
                Bangladesh
                <option value="srilanka"/>
                Srilanka
                </select>
            </td>
        </tr>

        <tr>
            <td><strong>Address:</strong></td>
            <td>
                <!-- TextArea -->
                <textarea rows="5" cols="20" name="myaddress" required="required"></textarea>
            </td>
        </tr>

        <tr>
            <td><strong>Username:</strong></td>
            <td>

                <!-- Textbox -->
                <input type="text" name="u_name" id="username" required="required" />
                <br>
                <span style="color: red; font-size: 12px;" id="usernameError"></span>
            </td>
        </tr>

        <tr>
            <td><strong>Password:</strong></td>
            <td>
                <!-- Password Field -->
                <input type="password" name="pass" id="password" required="required"  onkeyup="return passwordStrengthChecker();" />
                <br>
                <span id="strength">Type Password</span>
            </td>
        </tr>

        <tr>
            <td><strong>Retype Password:</strong></td>
            <td>
                <!-- Password Field -->
                <input type="password" name="r_pass" id="pass2" required="required" onkeyup="return passwordConfirm();" />
                <br>
                <span id="confirmation"></span>
            </td>
        </tr>

        <tr align="center">

            <td>
                <!--Submit Button, Function verify need to be called when we process
                submit button-->
                <input type="submit" value="Submit" onClick="return verify()"/>
            </td>

            <td>
                <!--Reset Button-->
                <input type="reset">
            </td>
        </tr>

        <!--Table Close-->
    </table>

    <!--Form Close -->
</form>


<script type="text/javascript">

/*function verify() {
    // e.preventDefault();
    // alert('here');
}*/

function usernameChecker(uniqueUsername) {
    var ok = true;
    $.get( "check_user_exist.php",
        { username: uniqueUsername },
        function( data ) {
            if(data == 1) {
                $('#usernameError').html('User already exist with this username.');
                ok = false;
            } else {
                ok = true;
                $('#usernameError').html('');
            }
    });

    return ok;
}
$('#username').on('blur', function() {
    var uniqueUsername = $(this).val();
    return usernameChecker(uniqueUsername);

});

/*$('#age').on('blur', function() {
    var text = '';
    var x = $(this).val();

    // If x is Not a Number or less than one or greater than 10
    if (isNaN(x)) {
        text = "Input not valid";
    } else {
        text = "";
    }
    $("#ageError").html(text);
});*/
</script>


<script language="javascript">
function passwordStrengthChecker() {
    var strength = document.getElementById('strength');
    var strongRegex = new RegExp("^(?=.{8,})(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*\W).*$", "g");
    var mediumRegex = new RegExp("^(?=.{7,})(((?=.*[A-Z])(?=.*[a-z]))|((?=.*[A-Z])(?=.*[0-9]))|((?=.*[a-z])(?=.*[0-9]))).*$", "g");
    var enoughRegex = new RegExp("(?=.{6,}).*", "g");
    var pwd = document.getElementById("password");
    if (pwd.value.length==0) {
        strength.innerHTML = 'Type Password';
    } else if (false == enoughRegex.test(pwd.value)) {
        strength.innerHTML = 'More Characters';
    } else if (strongRegex.test(pwd.value)) {
        strength.innerHTML = '<span style="color:green">Strong!</span>';
    } else if (mediumRegex.test(pwd.value)) {
        strength.innerHTML = '<span style="color:orange">Medium!</span>';
    } else {
        strength.innerHTML = '<span style="color:red">Weak!</span>';
    }
}
</script>

<script type="text/javascript">
function passwordConfirm() {
    var pass1 = document.getElementById("password").value;
    var pass2 = document.getElementById("pass2").value;
    var ok = true;
    var confirmation = document.getElementById("confirmation");

    if (pass1 != pass2) {
        //alert("Passwords Do not match");
        document.getElementById("password").style.borderColor = "#E34234";
        document.getElementById("pass2").style.borderColor = "#E34234";
        ok = false;
        confirmation.innerHTML = '<span style="color:orange">Password not matched!</span>';
    }
    else {
        confirmation.innerHTML = '<span style="color:green">Password matched!</span>';
    }
    return ok;
}
</script>

<script type="text/javascript">
    function verify() {

        var ok = true;

        if ($("form input:checkbox:checked").length) {
            // any one is checked
            ok = true;
            // console.log($("form input:checkbox:checked").length);
        } else {
            alert('You must need to check any of the language');
           // none is checked
            ok = false;
        }
        if( ! passwordConfirm()) {
            ok = false;
        }

        if( ! usernameChecker($('#username').val())) {
            ok = false;
        }

        return ok;
    }
</script>