<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Validate Me!</title>

        <!-- For Form validation we have to use JavaScript
        JavaScript needs only a browser to run, there is no need of 
        server to run JavaScript -->

        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        <script language="javascript">
        </script>
    </head>
    <body>
        <?php

            include('registration-form.php');
        ?>
    </body>
</html>