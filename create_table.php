<?php
        // sql to create table
        $sql = "CREATE TABLE $tableName (
        id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
        name VARCHAR(255) NOT NULL,
        email VARCHAR(50) NOT NULL,
        username VARCHAR(255) NOT NULL,
        password VARCHAR(64) NOT NULL,
        age VARCHAR(20),
        gender VARCHAR(20),
        language1 VARCHAR(50),
        language2 VARCHAR(50),
        language3 VARCHAR(50),
        country VARCHAR(50),
        address VARCHAR(255)
        )";

        // use exec() because no results are returned
        $conn->exec($sql);
        echo "Table $tableName created successfully";

        $conn = null;

        // connect database
        $conn = new PDO("mysql:host=$servername;dbname=$database",
                      $username, $password);

        // set the PDO error mode to exception
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);